/* Emily Kamleh - CS 474 Final
 *
 * Comments give definitions from the JLS, and, when needed, explain my implementation of these key concepts
 * from the JLS. I have included the relevant chapters in parenthesis next to their definitions.
 *
 */


import java.util.ArrayList;
import java.util.function.Function;

/*** Class Levels: Top, Nested (Member, Static Member, Local).. Anonymous in main ***/

/* Top Level Class (7.6) - a class that is not a nested class. */
/* This TopLevel Class has fields, methods, and three (3) different types of nested classes.
 * These include a Static Member Class, an inner Member Class, and a Local Class whose
 * scope is a method of TopLevel called MethodWithLocalClass. */
class TopLevel {
    /* Instance initialization */
    private int x = 5;
    private StaticMemberClass smc;
    private MemberClass mc;
    int num, num3, num4;
    /* Instance initializer (8.6) - block of executable code that may be used
     * to help initialize an instance when it is created. It is executed when an
     * instance of the class is created. */
    {num = 17; num3 = 34; num4 = 51;}

    /* Static initialization */
    private static int staticX;
    /* Static initializer (8.7) - block of executable code that may be used
     * to help initialize a class. It is executed when the class is initialized. */
    static {
        staticX = 17;
        System.out.println("Hello from the static block in TopClass");
    }

    /* Nested Class Type: Static Member Class */
    /* Static Member Class (8.1.3) - has no access to the instance variables of the surrounding class */
    static class StaticMemberClass {
        private int y;

        /* Constructor for StaticMemberClass */
        StaticMemberClass() {
            y = 14;
            System.out.println("Constructor called from StaticMemberClass");
            System.out.println("SMC: I do NOT have access to x.");
            System.out.println("I do have access to staticX though: " + staticX);

            /* To show that StaticMemberClass does not have access to TopLevelClass's instance variables,
             * you may uncomment the line below. This line is here merely as proof. */
            //System.out.println("Here is the value of x: " + x); //x turns red because StaticMemberClass does not have access to it
        }
        /* Method */
        public void staticMemberMethod() { System.out.println("SMC: Calling staticMemberMethod()"); }
    }

    /* Member Class (8.5) - nested classes that are members of the surrounding class. This one is an inner class (not static). */
    class MemberClass {
        /* Fields */
        private int y;

        /* Constructor (8.8) for MemberClass */
        MemberClass() {
            y = 14;
            System.out.println("Constructor called from MemberClass");
            System.out.println("MC: I DO have access to x. x is " + x);
        }

        /* Methods */
        public void MemberClassMethod() {
            System.out.println("MC: Calling MemberClassMethod()");
        }
    }

    /* TopLevel Constructor */
    TopLevel() {
        smc = new StaticMemberClass();
        mc = new MemberClass();
        System.out.println("Constructor called from TopLevel");
        System.out.println("TL: I own x. x is " + x);
    }

    /* Method Declarations */
    public MemberClass getMemberClass() {
        return mc;
    }
    public StaticMemberClass getStaticMemberClass() {
        return smc;
    }

    /* Nested Class Type: Local Class  */
    public void MethodWithLocalClass() {
        /* Local Class (14.3) - a nested class that is not a member of any class and that has a name.
         * Every local class declaration statement is immediately contained by a block. In this case,
         * I have declared my LocalClass inside a method. */
        class LocalClass {
            void printX() {
                System.out.println("LC: Printing x: " + x);
            }
        }
        LocalClass lc = new LocalClass();
        lc.printX();
    }

    /* Static Method (8.4.3.2) - a class method that is not invoked with a reference to a particular object. */
    public static int triple(int x) {
        return 3*x;
    }
}

/*** Generic, Final Class ***/

/* Parameterized Generic Final Class - This class is declared final (), is generic (), and parameterized ().
 * There are bounded type parameters that only allow this class to accept instances of Grandparent or its
 * subclasses. */
/* Generic Class (8.1.2) - a class that may declare type variables whose bindings may differ among different
 * instances of the class */
/* Final Class (8.1.1.2) - a class that cannot have subclasses */
final class GenericFinalClass<T extends Grandparent> {
    /* Member Declarations */

    /* Fields */
    protected T field1;
    private T field3;

    /* Methods */

    /* Getters */
    public T getField1() {
        return field1;
    }
    public T getField3() {
        return field3;
    }

    /* Parameterized Setter methods for field1 and field3
     * These are generic methods (8.4.4), as they take in type T  */
    public void setField1(T t) {
        field1 = t;
    }
    public void setField3(T t) { field3 = t; }

    /* Parameterized Constructor to make class parameterized */
    GenericFinalClass(T t1, T t3) {
        field1 = t1;
        field3 = t3;
        System.out.println("Constructor called from GenericClass with field1: " + field1 + " and field3: " + field3);
    }
}

/*** This area is designated to show Top Level Interfaces and Nested/Member Interfaces as well as an Abstract Class ***/

/* Top level interface (7.6) - not a nested interface */
/* Superinterface (8.1.5) - an interface that is implemented or extended.
 * This Interface is extended by MemberInterface in NestedInterfaceClass.
 * It is also implemented by AbstractClass below. */
interface Interface {
    /* These fields will be inherited by any class or interface that extends Interface */
    int implemented = 0;

    /* Methods */
    void interfaceMethod();
    void overloadThisMethod(int x);
    default void defaultMethod() {
        System.out.println("This is my default method. I defined it in myself.");
    }
    static void StaticInterfaceMethod() { System.out.println("This is my static method."); }
}

/* Functional Interface (9.8) - an interface that has just one abstract method, and thus represents a single function contract */
interface FunctionalInterface<T, U extends Number> {
    T calculate(U x, U y);
}

interface InstOfFunctionalInterface<T extends Object> {
    Boolean instOf(T x);
}

/* This is a class with a Nested Interface. */
class NestedInterfaceClass {
    /* Nested Interface (8.5) - nested interface that is a member of the surrounding class. */
    /* Subinterface (9.1.3) - an interface that extends another interface */
    protected interface MemberInterface extends Interface {
        void memberInterface();
    }
}

/* This is a class that implements from a Nested/Member Interface */
class TestMemberInterface implements NestedInterfaceClass.MemberInterface {
    public void interfaceMethod() {
        System.out.println("interfaceMethod() called from TestMemberInterface");
    }
    public void memberInterface() {
        System.out.println("I implement a Member Interface");
    }

    @Override
    public void overloadThisMethod(int x) {
        System.out.println("overloadThisMethod(int x) called from TestMemberInterface");
    }
}

/* Abstract Class (8.1.1.1) - an incompletely implemented class that cannot be instantiated, but can be
 * extended by subclasses. ONLY classes that are abstract can have abstract methods. */
/* This class implements an interface (8.1.5) */
abstract class AbstractClass implements Interface {
    /* Final Method (8.4.3.3) - a method that cannot be hidden or overridden */
    final void finalMethod() {
        System.out.println("This method is final and cannot be overridden.");
    }
    /* Abstract Methods */
    abstract void sayHello();

    @Override
    public void defaultMethod() {
        System.out.println("Calling defaultMethod() from AbstractClass. I overrode it.");
    }
}

/* (8.1.1.1) A subclass that is not itself abstract may be instantiated. */
class ExtendedAbstractClass extends AbstractClass {
    /* Overriding the Abstract Methods from AbstractClass */
    @Override
    void sayHello() {
        System.out.println("Hello from ExtendedAbstractClass");
    }
    @Override
    public void overloadThisMethod(int x) {
        System.out.println("Given an int x: " + x);
    }

    /* Overloaded overloadThisMethod() from NestedInterfaceClass.MemberInterface */
    void overloadThisMethod(double y) {
        System.out.println("Overloaded. Given a double y: " + y);
    }

    public void interfaceMethod() {
        System.out.println("Calling interfaceMethod from ExtendedAbstractClass");
    }
    public int getImplementedInt() {
        return implemented;
    }

    /* This commented out code below simply exists to show that a final method cannot be overridden. */
    //void finalMethod() { System.out.println("Darn, that didn't work"); } //red highlight because this is not allowed
}


/**** This region of code is used to show Inheritance. ****/

class Grandparent {
    /* Fields */
    protected int age;
    /* Field Hiding - The numChildren static variable is declared and defined in all of the subclasses,
     * so they are hidden from the immediate parent. */
    static int numChildren = 4;
    private String name;
    /* Final field (8.3.1.2) - can be assigned only once */
    final String species = "Human";

    /* Methods */
    public void askName() {
        System.out.println("What was that, sonny boy? I can't hear you.");
    }
    public void askAge() {
        System.out.println("Here, have some candy. Don't tell your mother.");
    }
    public int getAge() { return age; }
    public String getSpecies() { return species; }
    /* Method hiding */
    public static void sayGoodMorning() { System.out.println("Good morning!"); }

    /* Default Constructor (8.8) */
    Grandparent() {
        age = 128;
        name = "Granny";
    }
    /* Overloaded Grandparent Constructor (8.8.8) */
    Grandparent(int n, String s) {
        age = n;
        name = s;
    }
}

class Parent extends Grandparent {
    /* Private fields are NOT inherited by the subclasses, so they must also be declared in the subclasses.
     * In this case, the Parent does not get the age field from the Grandparent class, so we must have one in
     * here as well. Because the name is public, we inherit it.
     */
    private int age;
    /* Hiding of Class Variables */
    static int numChildren = 1;
    /* Hiding of Instance Variables from Grandparent */
    private String name = "Mum";

    /* Both askName() and askAge() are overridden so that they are different for the Parent class. */
    @Override
    public void askName() {
        System.out.println("My name is " + name);
    }
    @Override
    public void askAge() {
        System.out.println("That is none of your business.");
    }

    /* If we uncomment this, along with line [362], a Child invoking compareAgeToParent()
     * would get the correct result. */
    //public int getAge() { return age; }

    /* Implementing this method to demonstrate instance variable hiding. */
    public void compareAgeToParent() { System.out.println("My age: " + age + " parent's age: " + super.getAge()); }

    /* hiding of class variables */
    public void compareNumChildren() { System.out.println("I have " + numChildren + ". My parent has " + super.numChildren); }

    /* method hiding from Grandparent*/
    public static void sayGoodMorning() { System.out.println("Good..OH NO, I'M LATE FOR WORK!"); }

    /* Default Parent Constructor (8.8.8) */
    Parent() { age = 32; }

    /* Overloaded Parent Constructor (8.8.8) */
    Parent(String s) {
        age = 32;
        name = s;
    }
    Parent(int n, String s) {
        age = n;
        name = s;
    }
}

class Child extends Parent {
    /* Same explanation as above. Child does not inherit age, as it is declared private in its parent classes. */
    private int age;
    static int numChildren = 0;
    private String name;
    private String favoriteVideoGame;

    /* We override the askAge() method, which will allow us to answer differently than our Parent. See main, line (1) */
    @Override
    public void askAge() {
        System.out.println("I am " + age + " years old.");
    }

    /* Overloaded Method (8.4.9) - a method that has the same name as another method where their signatures are not
     * override-equivalent */
    /* This is an overloaded method that will allow the child to lie about their age. The signature is different from
     * askAge() above, as the overloaded one below now takes in an int n as a parameter. */
    public void askAge(int n) {
        System.out.println("I am " + n + " years old.");
    }
    /* Calling a static method to triple the age of the child. Could be useful in helping the child
     * lie about their age. Probably not that believable though. */
    public int tripleAge() {
        return TopLevel.triple(age);
    }
    public void askFavoriteGame() {
        System.out.println("My favorite video game is " + favoriteVideoGame);
    }
    public void compareNumChildren() { System.out.println("I have " + numChildren + ". My parent has " + super.numChildren); }
    /* Method hiding from Parent */
    public static void sayGoodMorning() { System.out.println("Ugh, it's too early!"); }

    /* If we uncomment the method below, we would get the correct values when a Child calls compareAgeToParent() */
    //public void compareAgeToParent() { System.out.println("My age: " + age + " parent's age: " + super.getAge()); }


    /* Default Child Constructor (8.8) */
    Child() {
        age = 8;
        favoriteVideoGame = "Jet Set Radio Future";
    }

    /* Overloaded Child Constructors (8.8.8) */
    Child(String s) {
        age = 8;
        name = s;
        favoriteVideoGame = "Jet Set Radio Future";

        /* This commented out code below exists to show that you cannot modify a final field's value: */
        //species = "Alien"; //red highlight because this is not allowed.
    }
    Child(int n, String s, String vg) {
        age = n;
        name = s;
        favoriteVideoGame = vg;
    }
}

class Unicode {

    public void printString(int c) {
        String s = Character.toString((char)c);
        System.out.println("Here is the String: " + s);
    }
}

/* Shadowing */
class Shadow {
    /* Instance variable */
    private String favoriteHero = "Batman";

    Shadow() {
        /* Constructor local variable is shadowing the instance variable */
        String favoriteHero = "Wonder Woman";
        System.out.println("favoriteHero: " + favoriteHero);
    }

    public void printSpiderMan() {
        /* Method local variable is shadowing the instance variable */
        String favoriteHero = "Spider-man";
        System.out.println("favoriteHero: " + favoriteHero);
    }

    public void printInstanceVariable() {
        /* Method local variable is shadowing the instance variable */
        String favoriteHero = "Flash";
        System.out.println("favoriteHero: " + this.favoriteHero);
    }
}

class Switch {
    String color = null;

    void getColor(String SonicCharacter) {
        switch(SonicCharacter) {
            case "Sonic":
                color = "Blue";
                break;
            case "Knuckles":
                color = "Red";
                break;
            case "Tails":
                color = "Yellow";
                break;
            case "Shadow":
                color = "Black";
                break;
            case "Rouge":
                color = "White";
                break;
            case "Amy":
                color = "Pink";
                break;
            case "Big":
                color = "Purple";
                break;
            default:
                color = "Invalid Sonic Character";
        }
        System.out.println(SonicCharacter + " is " + color);
    }
}

class GradeCalculator {
    char grade;

    public char CalculateGrade(int score) {
        /* If-Then...Then-Else Statement (14.9.2) */
        if (score >= 90) {
            grade = 'A';
        } else if (score >= 80) {
            grade = 'B';
        } else if (score >= 70) {
            grade = 'C';
        } else if (score >= 60) {
            grade = 'D';
        } else {
            grade = 'F';
        }
        System.out.println("My final grade is (hopefully) a(n) " + grade);
        return grade;
    }
}


/* Public Class - can be referred to from code in any package of its module and potentially from code in other modules. */
public class Final {
    public static void main(String[] args) {
        /* This area is calling a static method. */
        Interface.StaticInterfaceMethod();
        System.out.println("--------------------------------------");

        /* This area: Overloading an Interface method */
        ExtendedAbstractClass eac = new ExtendedAbstractClass();
        eac.overloadThisMethod(4);
        eac.overloadThisMethod(4.5);
        System.out.println("-----------------------------------------------------------");

        /* Shadowing - the value of a field defined in multiple scopes
         * depends on the scope we are currently in */
        Shadow shadow = new Shadow();
        shadow.printSpiderMan();
        shadow.printInstanceVariable();
        System.out.println("-----------------------------------------------------------");

        /* Interfaces */

        TestMemberInterface tmi = new TestMemberInterface();
        tmi.interfaceMethod();
        System.out.println("-----------------------------------------------------------");

        /* This area: Subtyping in a Generic Class */

        GenericFinalClass<Grandparent> gfc = new GenericFinalClass<Grandparent>(new Child("Tim Drake"), new Parent("Bruce Wayne"));
        gfc.getField1().askName();
        gfc.getField1().askAge();
        gfc.getField3().askName();
        gfc.getField3().askAge();
        System.out.println("-----------------------------------------------------------");

        /* This area of main refers to Top Level classes, Nested Classes (Member, Local, and Anonymous). */

        TopLevel tl = new TopLevel();
        tl.getMemberClass().MemberClassMethod();
        tl.MethodWithLocalClass();
        tl.getStaticMemberClass().staticMemberMethod();
        System.out.println("-----------------------------------------------------------");

        /* This area of main refers to Inheritance and class hierarchies */
        /* Mainly, the topics shown here are overridden methods,  */

        System.out.println("Granny:");
        Grandparent granny = new Grandparent();
        granny.askName();
        granny.askAge();
        granny.sayGoodMorning();
        System.out.println("-----------------------------------------------------------");

        System.out.println("Mum:");
        Parent mum = new Parent();
        mum.askName();
        mum.compareAgeToParent();
        mum.compareNumChildren();
        mum.askAge();
        mum.sayGoodMorning();

        System.out.println("-----------------------------------------------------------");

        System.out.println("Kid:");
        Child kid = new Child();
        kid.askName();
        kid.askAge(); // (1)
        kid.askFavoriteGame();
        kid.askAge(kid.tripleAge());
        kid.getSpecies();
        kid.compareAgeToParent();
        kid.compareNumChildren();
        kid.sayGoodMorning();
        System.out.println("-----------------------------------------------------------");

        /* Notice everyone's name is Granny here.
         * Because I called the default constructors, all of the subclasses inherited their names from
         * the superclass Granny. If you use any of the overloaded Constructors, this will not happen. */

        /* This area refers to Polymorphism. It covers:
         * - Method Overloading/Compile-time (or Static) Polymorphism () -
         * - Method Overriding/Runtime (or Dynamic) Polymorphism () -
         * - FIGURE OUT WHAT THIS IS :/
         */

        System.out.println("Polymorphism:");

        /*** In this region, we will use the Overloaded Constructors ***/

        /* In this example, we specify the type of mum as Grandparent, but we call the Parent constructor.
         * Because of this, the methods that are actually used are the ones that belong to the Parent class. */

        System.out.println("Grandparent dad = new Parent(46, Bruce Wayne):");
        Grandparent dad = new Parent(46, "Bruce Wayne");
        dad.askAge();
        dad.askName();
        dad.sayGoodMorning();
        System.out.println("-----------------------------------------------------------");

        /* As mentioned above, even though we called the Child constructor to create the dad3 instance,
         * we are unable to call the askFavoriteGame() method. */

        System.out.println("Grandparent dad3 = new Child(13, Zelda, Sonic Heroes):");
        Grandparent dad3 = new Child(13, "Zelda", "Sonic Heroes");
        dad3.askAge();
        dad3.askName();
        dad3.sayGoodMorning();
        //dad.compareAgeToParent();
        //dad.compareNumChildren();
        System.out.println("-----------------------------------------------------------");

        /* In this example, we specify the type of son to be Parent, but we call the Child constructor.
         * Because of this, our output will be */

        System.out.println("Parent son = new Child():");
        Parent son = new Child(21, "Jason Todd", "Arkham Knight");
        son.askAge();
        son.sayGoodMorning();
        son.compareAgeToParent();
        son.compareNumChildren();
        son.askName();

        /* Because we declared son as a Parent, we cannot call the method below. The commented out code reveals
         * that there is no askFavoriteGame() method for the Parent class. */
        //son.askFavoriteGame();
        System.out.println("-----------------------------------------------------------");

        /* The commented out code below shows things we CANNOT do with inheritance.
         * You cannot call the constructor of a superclass to create a subclass. */

        //Parent parent = new Grandparent();
        //Child child = new Grandparent();
        //Child child = new Parent();

        /*** Functional Interface (9.8) ***/

        System.out.println("Functional Interfaces:");
        FunctionalInterface<Integer, Integer> fiAdd = (Integer x, Integer y) -> x + y;
        Integer addResult = fiAdd.calculate(4, 5);
        System.out.println("The result is " + addResult);
        System.out.println("-----------------------------------------------------------");

        FunctionalInterface<Double, Double> fiSub = (Double x, Double y) -> x - y;
        Double subResult = fiSub.calculate(17.0, 13.0);
        System.out.println("The result is " + subResult);
        System.out.println("-----------------------------------------------------------");

        Float a = 17.0f, b = 4.0f;
        FunctionalInterface<Boolean, Float> fiBool = (Float x, Float y) -> x != y;
        Boolean neqResult = fiBool.calculate(a, b);
        System.out.println(a + " != " + b + "? " + neqResult);
        System.out.println("-----------------------------------------------------------");

        Integer thing1 = 0x101, thing3 = 0x011;
        FunctionalInterface<Integer, Integer> fiAnd3 = (Integer x, Integer y) -> x & y;
        Integer andResult3 = fiAnd3.calculate(thing1, thing3);
        System.out.println(thing1 + " & " + thing3 + " is " + andResult3);
        System.out.println("-----------------------------------------------------------");

        System.out.println("Using instanceof in a lambda function:");
        Integer instInt = 5;
        InstOfFunctionalInterface<Integer> fiInstOf = (Integer x) -> x instanceof Number;
        Boolean instOfResult = fiInstOf.instOf(instInt);
        System.out.println(instInt.getClass() + " instance of Number ? " + instOfResult);
        System.out.println("-----------------------------------------------------------");

        System.out.println("Anonymous Class:");
        /* Anonymous class (15.9.5) - a local class without a name */
        ExtendedAbstractClass anonymous = new ExtendedAbstractClass() {
            void sayHello() {System.out.println("Hello from Anonymous");}
        };
        anonymous.sayHello();
        System.out.println("-----------------------------------------------------------");

        System.out.println("An attept to play around with Unicode:");
        Unicode oi = new Unicode();
        oi.printString(0x0041);
        System.out.println("-----------------------------------------------------------");

        System.out.println("Switch Statement:");
        Switch myColor = new Switch();
        myColor.getColor("Knuckles");
        System.out.println("-----------------------------------------------------------");

        System.out.println("If-Then-Else Statements:");
        GradeCalculator gc = new GradeCalculator();
        gc.CalculateGrade(97);
        System.out.println("-----------------------------------------------------------");

        System.out.println("Creating a Function by putting two other Functions together:");
        Function<Integer, Integer> addFive = (value) -> value + 5;
        Function<Integer, Integer> multiplyByThree = (value) -> 3 * value;

        Function<Integer, Integer> addThenMultiply = multiplyByThree.compose(addFive);
        Integer atmResult = addThenMultiply.apply(4);
        System.out.println("addThenMultiply.apply(4): " + atmResult);
        System.out.println("-----------------------------------------------------------");

        System.out.println("Abstract Class and Extended Abstract Class:");
        AbstractClass ac = new ExtendedAbstractClass();
        ExtendedAbstractClass eac3 = new ExtendedAbstractClass();
        ac.sayHello();
        eac3.sayHello();
        System.out.println("The implementedInt is " + eac.getImplementedInt());


       /* Obscuring */
       //String System = "Sanic";
       //System.out.println(System);
    }
}
